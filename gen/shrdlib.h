/**
 * @file
 * @brief     Shared library loader.
 * @details   Encapsulation of shared library loader.
 * @author    王文佑
 * @date      2014.01.25
 * @copyright ZLib Licence
 * @see       http://www.openfoundry.org/of/projects/2419
 */
#ifndef _GNE_SHRDLIB_H_
#define _GEN_SHRDLIB_H_

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @file
 * @note User will need to link "dl" library (-ldl) in Linux.
 */

/// Library handler.
typedef void* shrdlib_t;

shrdlib_t shrdlib_open(const char *filename, char *errmsg_buf, size_t errmsg_bufsize);
bool      shrdlib_close(shrdlib_t handle);
void*     shrdlib_getfunc(shrdlib_t handle, const char *funcname);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
