#include <string.h>

#ifdef __linux__
#include <dlfcn.h>  // Also remember to link "dl" library (-ldl)
#endif

#ifdef _WIN32
#include <windows.h>
#include <utfconv/utfconv.h>
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "shrdlib.h"

//------------------------------------------------------------------------------
shrdlib_t shrdlib_open(const char *filename, char *errmsg_buf, size_t errmsg_bufsize)
{
    /**
     * Load a dynamic shared library.
     * @param filename          File name of the dynamic shared library.
     * @param errmsg_buf        A buffer to receive human readable text if load failed.
     *                          This parameter can be NULL if not needed.
     * @param errmsg_bufsize    Size of the error message buffer.
     * @return A library handle if succeed; and NULL if failed.
     */
    shrdlib_t handle = NULL;

#if   defined(__linux__)
    handle = dlopen(filename, RTLD_LAZY);
    if( !handle && errmsg_buf && errmsg_bufsize )
    {
        strncpy(errmsg_buf, dlerror(), errmsg_bufsize);
        errmsg_buf[errmsg_bufsize-1] = 0;
    }
#elif defined(_WIN32)
    do
    {
        TCHAR winname[260];

        if( errmsg_buf && errmsg_bufsize )
            errmsg_buf[0] = 0;
        if( !utfconv_utf8_to_winstr(winname, sizeof(winname), filename) )
            break;

        handle = LoadLibrary(winname);
        if( !handle && errmsg_buf && errmsg_bufsize )
        {
            DWORD errcode = GetLastError();

            LPTSTR tstr = NULL;
            DWORD size =
                FormatMessage(
                    FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                    NULL,
                    errcode,
                    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                    (LPTSTR) &tstr,
                    0,
                    NULL);

            utfconv_winstr_to_utf8(errmsg_buf, errmsg_bufsize-1, tstr)

            LocalFree(tstr);
        }
    } while(false);
#else
    #error No implementation on this platform!
#endif

    return handle;
}
//------------------------------------------------------------------------------
bool shrdlib_close(shrdlib_t handle)
{
    /**
     * Unload a dynamic shared library.
     * @param handle A handler of the loaded library.
     * @return TRUE if succeed; and FALSE if failed.
     */
#if   defined(__linux__)
    return !dlclose(handle);
#elif defined(_WIN32)
    return FreeLibrary((HMODULE)handle);
#else
    #error No implementation on this platform!
#endif
}
//------------------------------------------------------------------------------
void* shrdlib_getfunc(shrdlib_t handle, const char *funcname)
{
    /**
     * Get the function address of the library.
     * @param handle   A handler of the loaded library.
     * @param funcname Name of the function exported form the library.
     * @return Address of the symbol; and NULL if failed.
     */
#if   defined(__linux__)
    return dlsym(handle, funcname);
#elif defined(_WIN32)
    return GetProcAddress((HMODULE)handle, funcname);
#else
    #error No implementation on this platform!
#endif
}
//------------------------------------------------------------------------------
