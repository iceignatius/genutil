#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifdef _WIN32
#include <utfconv/utfconv.h>
#endif

#include "minmax.h"
#include "membuf.h"

//------------------------------------------------------------------------------
//---- Tools -------------------------------------------------------------------
//------------------------------------------------------------------------------

void* memfind(const void *src, size_t srcsz, const void *pattern, size_t patsz)
{
    /**
     * 在一段記憶體資料中從頭開始搜尋與指定資料吻合的位置。
     * @param src     被搜尋的緩衝區。
     * @param srcsz   被搜尋緩衝區大小。
     * @param pattern 搜尋目標。
     * @param patsz   搜尋目標大小。
     * @return 返回第一個搜尋到資料吻合的資料指標；若搜尋失敗則反回 NULL。
     */
    uint8_t *dat;
    size_t  sizepass;

    while( true )
    {
        dat = memchr(src, *(uint8_t*)pattern, srcsz);
        if( !dat ) break;
        sizepass = (size_t)dat - (size_t)src + 1;
        if( sizepass > srcsz ) break;

        if( 0 == memcmp(dat, pattern, patsz) ) return dat;

        src    = (uint8_t*)src + sizepass;
        srcsz -= sizepass;
    }

    return NULL;
}

void* memrfind(const void *src, size_t srcsz, const void *pattern, size_t patsz)
{
    /**
     * 在一段記憶體資料中從結尾開始搜尋與指定資料吻合的位置。
     * @param src     被搜尋的緩衝區。
     * @param srcsz   被搜尋緩衝區大小。
     * @param pattern 搜尋目標。
     * @param patsz   搜尋目標大小。
     * @return 返回第一個搜尋到資料吻合的資料指標；若搜尋失敗則反回 NULL。
     */
    uint8_t *dat = (uint8_t*)src + srcsz - patsz;

    while( true )
    {
        if(( *dat == *(uint8_t*)pattern )&&( 0 == memcmp(dat, pattern, patsz) )) return dat;
        if( dat == src ) break;
        --dat;
    }

    return NULL;
}

unsigned memfindcount(const void *src, size_t srcsz, const void *pattern, size_t patsz)
{
    /**
     * 搜尋一段記憶體資料中吻合某目標資料的片斷數量。
     * @param src     被搜尋的緩衝區。
     * @param srcsz   被搜尋緩衝區大小。
     * @param pattern 搜尋目標。
     * @param patsz   搜尋目標大小。
     * @return 返回緩衝區中吻合指定資料的片斷數量。
     */
    unsigned count = 0;

    const uint8_t *pos0 = src;
    const uint8_t *pos;
    while(( pos = memfind(pos0, srcsz, pattern, patsz) ))
    {
        ++count;

        size_t size = pos - pos0 + patsz;
        pos0  += size;
        srcsz -= size;
    }

    return count;
}

size_t memfindreplace(
    void        *dest,
    size_t      destsz,
    const void  *src,
    size_t      srcsz,
    const void  *pattern,
    size_t      patsz,
    const void  *target,
    size_t      tarsz)
{
    /**
     * 搜尋一段記憶體中的特定資料，並將之取代為另一個指定的資料。
     * @param dest    接收結果資料的緩衝區。
     * @param destsz  接收結果資料的緩衝區大小。
     * @param src     被搜尋的緩衝區。
     * @param srcsz   被搜尋緩衝區大小。
     * @param pattern 搜尋目標。
     * @param patsz   搜尋目標大小。
     * @param target  取代搜尋目標的資料。
     * @param tarsz   取代搜尋目標的資料大小。
     * @return 成功時返回已寫入 @a dest 的資料數量；
     *         或當 @a dest 為 NULL 時返回輸出緩衝區所需的最小大小；
     *         或失敗時返回零。
     */
    uint8_t         *_dest  = dest;
    const uint8_t   *_src   = src;

    unsigned findcnt  = memfindcount(_src, srcsz, pattern, patsz);
    size_t   sizeneed = (long long)srcsz + findcnt*( (long long)tarsz - (long long)patsz );
    if( !_dest ) return sizeneed;
    if( destsz < sizeneed ) return 0;

    const uint8_t *pos;
    do
    {
        pos = memfind(_src, srcsz, pattern, patsz);
        if( pos )
        {
            size_t blksz = pos - _src;
            memcpy(_dest, _src, blksz);
            _dest  += blksz;
            destsz -= blksz;

            memcpy(_dest, target, tarsz);
            _dest  += tarsz;
            destsz -= tarsz;

            blksz += patsz;
            _src  += blksz;
            srcsz -= blksz;
        }
        else
        {
            memcpy(_dest, _src, srcsz);
        }

    } while( pos );

    return sizeneed;
}

size_t membuf_calc_recommended_size(size_t size)
{
    /*
     * 計算大於指定大小的最適合之緩衝區配置尺寸。
     */
    static const size_t size_max = ((size_t)-1)>>1;

    if     ( size > MEMBUF_BLOCKSZ_LARGE/2 )
    {
        if( size < size_max )
            size = ( size + MEMBUF_BLOCKSZ_LARGE - 1 ) & ~( MEMBUF_BLOCKSZ_LARGE - 1 );
    }
    else if( size > MEMBUF_BLOCKSZ_SMALL )
    {
        size = ( size + MEMBUF_BLOCKSZ_SMALL - 1 ) & ~( MEMBUF_BLOCKSZ_SMALL - 1 );
    }
    else
    {
        size = MEMBUF_BLOCKSZ_SMALL;
    }

    return size;
}

static
FILE* file_open_readonly(const char *filename)
{
    assert( filename );

#if defined(_WIN32) && defined(_UNICODE)
    wchar_t wcsname[260];
    if( !utfconv_utf8_to_wcs(wcsname, sizeof(wcsname), filename) )
        return NULL;
    return _wfopen(wcsname, L"rb");
#else
    return fopen(filename, "rb");
#endif
}

static
FILE* file_open_writable(const char *filename)
{
    assert( filename );

#if defined(_WIN32) && defined(_UNICODE)
    wchar_t wcsname[260];
    if( !utfconv_utf8_to_wcs(wcsname, sizeof(wcsname), filename) )
        return NULL;
    return _wfopen(wcsname, L"wb");
#else
    return fopen(filename, "wb");
#endif
}

static
FILE* file_open_writable_text(const char *filename)
{
    assert( filename );

#if defined(_WIN32) && defined(_UNICODE)
    wchar_t wcsname[260];
    if( !utfconv_utf8_to_wcs(wcsname, sizeof(wcsname), filename) )
        return NULL;
    return _wfopen(wcsname, L"w");
#else
    return fopen(filename, "w");
#endif
}

static
long file_get_size(FILE *file)
{
    /*
     * Get file size.
     * @param file Input a file handler.
     * @return A value great equal to zero if succeed;
     *         and a negative value if failed.
     * @note This function will change position of the file read-write head.
     */
    assert( file );

    if( fseek(file,0,SEEK_END) ) return -1;
    return ftell(file);
}

static
size_t file_read_binary(FILE *file, uint8_t *buffer, size_t size)
{
    assert( file );

    if( fseek(file,0,SEEK_SET) ) return 0;
    return fread(buffer, 1, size, file);
}

static
bool file_write_binary(FILE *file, const uint8_t *data, size_t size)
{
    assert( file && data );

    return size == fwrite(data, 1, size, file);
}

static
void file_write_text(FILE *file, const uint8_t *data, size_t size)
{
    /*
     * Translate data to text format and write to file.
     */
    static const char* const textgap = "    ";
    size_t i;

    assert( file && data );

    // Output data size
    fprintf(file, "Total Size : %lu (bytes)\n\n", (unsigned long)size);
    // Output address position label
    if( size )
    {
        fprintf(file,
                "%8s%s%s%s%s\n\n",
                " ",
                textgap,
                "00 01 02 03 04 05 06 07",
                textgap,
                "08 09 0A 0B 0C 0D 0E 0F");
    }
    // Output all binary data to text format
    for(i=0; i<size; ++i)
    {
        if( ( ( i ) % 16 ) == 0 ) fprintf(file, "%8.8lX%s", (unsigned long)i, textgap);

        fprintf(file, "%2.2X", (unsigned)data[i]);

        if     ( ( (i+1) % 16 ) == 0 ) fprintf(file, "\n");
        else if( ( (i+1) %  8 ) == 0 ) fprintf(file, "%s", textgap);
        else                           fprintf(file, " ");
    }
    if( ( i % 16 ) != 0 ) fprintf(file, "\n");
}

//------------------------------------------------------------------------------
//---- Memory Buffer -----------------------------------------------------------
//------------------------------------------------------------------------------

static
void abort_if_alloc_fail(const void *buf)
{
    if( !buf )
    {
        fputs("ERROR: Memory allocation failed!", stderr);
        abort();
    }
}

void membuf_init(membuf_t *self, size_t size)
{
    /**
     * @memberof membuf
     * @brief Constructor.
     * @details Initialise object with an initial buffer size.
     *
     * @param self Object instance.
     * @param size The initial buffer size.
     */
    assert( self );

    self->size_total = membuf_calc_recommended_size(size);
    assert( self->size_total >= size );

    self->buf_total = malloc(self->size_total);
    abort_if_alloc_fail(self->buf_total);

    self->size  = size;
    self->buf   = self->buf_total;
}

void membuf_init_import(membuf_t *self, const void *buffer, size_t size)
{
    /**
     * @memberof membuf
     * @brief Constructor.
     * @details Initialise object and import data from another buffer.
     *
     * @param self   Object instance.
     * @param buffer Data to import from.
     * @param size   Size of data to import.
     */
    assert( self );

    size = buffer ? size : 0;
    membuf_init(self, size);
    memcpy(self->buf, buffer, size);
}

void membuf_init_clone(membuf_t *self, const membuf_t *src)
{
    /**
     * @memberof membuf
     * @brief Constructor.
     * @details Initialise object and import data from another @ref membuf_t object.
     *
     * @param self Object instance.
     * @param src  Data to import from.
     */
    assert( self && src );

    membuf_init(self, src->size);
    memcpy(self->buf, src->buf, src->size);
}

void membuf_init_move(membuf_t *self, membuf_t *src)
{
    /**
     * @memberof membuf
     * @brief Constructor.
     * @details Initialise object and move data from another @ref membuf_t object.
     *
     * @param self Object instance.
     * @param src  Data to move from.
     */
    assert( self && src );

    *self = *src;
    membuf_init(src, 0);
}

void membuf_destroy(membuf_t *self)
{
    /**
     * @memberof membuf
     * @brief Destructor.
     */
    assert( self );
    free(self->buf_total);
}

membuf_t* membuf_create(size_t size)
{
    /**
     * @memberof membuf
     * @static
     * @brief 創建動態的物件。
     *
     * @param size The initial buffer size.
     * @return The object that created if succeed; and NULL if failed.
     */
    membuf_t *inst = malloc(sizeof(membuf_t));
    if( !inst ) return NULL;

    inst->size_total = membuf_calc_recommended_size(size);
    assert( inst->size_total >= size );

    inst->buf_total = malloc(size);
    if( !inst->buf_total )
    {
        free(inst);
        return NULL;
    }

    inst->size  = size;
    inst->buf   = inst->buf_total;

    return inst;
}

membuf_t* membuf_create_import(const void *buffer, size_t size)
{
    /**
     * @memberof membuf
     * @static
     * @brief 創建動態的物件，並從其他緩衝區匯入資料。
     *
     * @param buffer Data to import from.
     * @param size   Size of data to import.
     * @return The object that created if succeed; and NULL if failed.
     */
    size = buffer ? size : 0;

    membuf_t *inst = membuf_create(size);
    if( !inst ) return NULL;

    memcpy(inst->buf, buffer, size);

    return inst;
}

membuf_t* membuf_create_clone(const membuf_t *src)
{
    /**
     * @memberof membuf
     * @static
     * @brief 創建動態的物件，並從其他 mem 物件複製資料。
     *
     * @param src Data to import from.
     * @return The object that created if succeed; and NULL if failed.
     */
    if( !src ) return NULL;

    membuf_t *inst = membuf_create(src->size);
    if( !inst ) return NULL;

    memcpy(inst->buf, src->buf, src->size);

    return inst;
}

membuf_t* membuf_create_move(membuf_t *src)
{
    /**
     * @memberof membuf
     * @static
     * @brief 創建動態的物件，並從其他 mem 物件搬移資料。
     *
     * @param src Data to move from.
     * @return The object that created if succeed; and NULL if failed.
     */
    if( !src ) return NULL;

    membuf_t *inst = membuf_create(0);
    if( !inst ) return NULL;

    membuf_t temp = *inst;
    *inst = *src;
    *src = temp;

    return inst;
}

membuf_t* membuf_create_load_file(const char *filename)
{
    /**
     * @memberof membuf
     * @static
     * @brief 創建動態的物件，並從檔案讀入資料。
     *
     * @param filename The name of file to load data from.
     * @return The object that created if succeed; and NULL if failed.
     */
    membuf_t *inst = membuf_create(0);
    if( inst )
    {
        if( !membuf_load_file(inst, filename) )
            membuf_release_s(&inst);
    }
    return inst;
}

void membuf_release(membuf_t *self)
{
    /**
     * @memberof membuf
     * @brief 釋放動態的物件。
     *
     * @param self Object instance.
     */
    if( self )
    {
        membuf_destroy(self);
        free(self);
    }
}

void membuf_release_s(membuf_t **self)
{
    /**
     * @memberof membuf
     * @brief 釋放動態的物件，並重設物件指標為NULL。
     *
     * @param self Reference of the object instance.
     */
    if( self )
    {
        membuf_release(*self);
        *self = NULL;
    }
}

void membuf_set_zeros(membuf_t *self)
{
    /**
     * @memberof membuf
     * @brief 將緩衝區的資料全設定為零。
     *
     * @param self Object instance.
     */
    if( self ) memset(self->buf, 0, self->size);
}

static
void membuf_remove_buffer_unused_leading_spaces(membuf_t *self)
{
    if( !self ) return;
    if( self->buf == self->buf_total ) return;

    assert( self->buf >= self->buf_total );
    assert(
        self->size_total >=
        (size_t)( /* leading size */ self->buf - self->buf_total ) );

    memmove(self->buf_total, self->buf, self->size);
    self->buf = self->buf_total;
}

bool membuf_resize(membuf_t *self, size_t size)
{
    /**
     * @memberof membuf
     * @brief 變更緩衝區大小。
     *
     * @param self Object instance.
     * @param size New size of the data buffer.
     * @return TRUE if succeed; and FALSE if failed.
     *
     * @remarks Data in the buffer will be saved if the new size is great equal than the old;
     *          and will be truncated if the new size is less than the old.
     */
    if( !self ) return false;

    assert( self->buf >= self->buf_total );
    size_t leading_size = self->buf - self->buf_total;
    assert( self->size_total >= leading_size );

    if( self->size_total - leading_size >= size )
    {
        self->size = size;
        return true;
    }

    if( leading_size )
    {
        membuf_remove_buffer_unused_leading_spaces(self);
        if( self->size_total >= size )
        {
            self->size = size;
            return true;
        }
    }

    size_t newsize = membuf_calc_recommended_size(size);
    assert( newsize >= size );

    uint8_t *newbuf = realloc(self->buf, newsize);
    if( !newbuf ) return false;

    self->size_total    = newsize;
    self->buf_total     = newbuf;
    self->size          = size;
    self->buf           = self->buf_total;

    return true;
}

void membuf_clear(membuf_t *self)
{
    /**
     * @memberof membuf
     * @brief 清空緩衝區。
     *
     * @param self Object instance.
     */
    if( !self ) return;

    self->buf   = self->buf_total;
    self->size  = 0;
}

bool membuf_import(membuf_t *self, const void *buffer, size_t size)
{
    /**
     * @memberof membuf
     * @brief 將一段資料內容設定至物件緩衝區內。
     *
     * @param self   Object instance.
     * @param buffer Data to import from.
     * @param size   Size of data to import.
     * @return TRUE if succeed; and FALSE if failed.
     */
    if( !self || !buffer ) return false;

    if( !membuf_resize(self, size) ) return false;
    memcpy(self->buf, buffer, size);

    return true;
}

bool membuf_append(membuf_t *self, const void *buffer, size_t size)
{
    /**
     * @memberof membuf
     * @brief 將一段資料添加至物件緩衝區原有資料的後面。
     *
     * @param self   Object instance.
     * @param buffer Data to append.
     * @param size   Size of data to append.
     * @return TRUE if succeed; and FALSE if failed.
     */
    size_t sizeold;

    if( !self || !buffer ) return false;

    sizeold = self->size;
    if( !membuf_resize(self, sizeold+size) ) return false;
    memcpy(self->buf+sizeold, buffer, size);

    return true;
}

void membuf_move_from(membuf_t *self, membuf_t *src)
{
    /**
     * @memberof membuf
     * @brief 物件資料搬移。
     *
     * @param self Object instance.
     * @param src  Data to move from.
     * @return TRUE if succeed; and FALSE if failed.
     */
    if( !self || !src ) return;

    membuf_destroy(self);
    *self = *src;
    membuf_init(src, 0);
}

void membuf_pop_front(membuf_t *self, size_t popsz)
{
    /**
     * @memberof membuf
     * @brief 將緩衝區頭部的一段資料移除，並將其他資料前移。
     *
     * @param self  Object instance.
     * @param popsz Size of data to pop.
     */
    if( !self ) return;

    if( popsz < self->size )
    {
        self->buf   += popsz;
        self->size  -= popsz;
    }
    else
    {
        self->buf   = self->buf_total;
        self->size  = 0;
    }
}

int membuf_compare(const membuf_t *self, const membuf_t *tar)
{
    /**
     * @memberof membuf
     * @brief 比較兩個緩衝區的資料。
     *
     * @param self Object instance.
     * @param tar  An other object to be compared.
     * @retval negative The data size of current object is shorter than the other, or
                        the first byte that does not match in both data blocks
     *                  has a lower value in the current object than the other.
     * @retval zero     The contents of both object are equal.
     * @retval positive The data size of current object is larger than the other, or
     *                  the first byte that does not match in both data blocks
     *                  has a greater value in the current object than the other.
     */
    if( !self ) return -1;
    if( !tar ) return 1;

    if( self->size != tar->size ) return self->size < tar->size ? -1 : 1;
    return memcmp(self->buf, tar->buf, self->size);
}

const void* membuf_find(const membuf_t *self, const membuf_t *pattern)
{
    /**
     * @memberof membuf
     * @see ::memfind
     */
    return ( self && pattern )?
           ( memfind(self->buf, self->size, pattern->buf, pattern->size) ):
           ( NULL );
}

const void* membuf_rfind(const membuf_t *self, const membuf_t *pattern)
{
    /**
     * @memberof membuf
     * @see ::memrfind
     */
    return ( self && pattern )?
           ( memrfind(self->buf, self->size, pattern->buf, pattern->size) ):
           ( NULL );
}

unsigned membuf_find_count(const membuf_t *self, const membuf_t *pattern)
{
    /**
     * @memberof membuf
     * @see ::memfindcount
     */
    return ( self && pattern )?
           ( memfindcount(self->buf, self->size, pattern->buf, pattern->size) ):
           ( 0 );
}

bool membuf_find_replace(membuf_t *self, const membuf_t *pattern, const membuf_t *target)
{
    /**
     * @memberof membuf
     * @brief 搜尋一段記憶體中的特定資料，並將之取代為另一個指定的資料。
     *
     * @param self    Object instance.
     * @param pattern Search pattern.
     * @param target  Data to replace search pattern.
     * @return TRTUE if succeed; and FALSE if failed.
     */
    if( !self || !pattern || !target ) return false;

    membuf_t src;
    membuf_init(&src, 0);

    bool res = false;
    do
    {
        if( !membuf_import(&src, self->buf, self->size) ) break;

        size_t sizeneed = memfindreplace(NULL,
                                         0,
                                         src.buf,
                                         src.size,
                                         pattern->buf,
                                         pattern->size,
                                         target->buf,
                                         target->size);
        if( !sizeneed || !membuf_resize(self, sizeneed) ) break;

        size_t sizefinal = memfindreplace(self->buf,
                                          self->size,
                                          src.buf,
                                          src.size,
                                          pattern->buf,
                                          pattern->size,
                                          target->buf,
                                          target->size);
        if( !sizefinal || !membuf_resize(self, sizefinal) ) break;

        res = true;
    } while(false);

    membuf_destroy(&src);

    return res;
}

bool membuf_save_file(const membuf_t *self, const char *filename)
{
    /**
     * @memberof membuf
     * @brief 將緩衝區內容存入檔案。
     *
     * @param self     Object instance.
     * @param filename Name of file to save data.
     * @return TRUE if succeed; and FALSE if failed.
     */
    FILE *file    = NULL;
    bool  succeed = false;

    if( !self || !filename ) return false;

    do
    {
        file = file_open_writable(filename);
        if( !file ) break;

        if( !file_write_binary(file, self->buf, self->size) ) break;

        succeed = true;
    } while(false);

    if( file ) fclose(file);

    return succeed;
}

bool membuf_load_file(membuf_t *self, const char *filename)
{
    /**
     * @memberof membuf
     * @brief 將檔案內容讀入至緩衝區。
     *
     * @param self     Object instance.
     * @param filename Name of file to load data.
     * @return TRUE if succeed; and FALSE if failed.
     */
    FILE        *file       = NULL;
    membuf_t    *memtemp    = NULL;
    bool        succeed     = false;

    if( !self || !filename ) return false;

    do
    {
        long   filesz;
        size_t recsz;

        file = file_open_readonly(filename);
        if( !file ) break;

        filesz = file_get_size(file);
        if( filesz < 0 ) break;

        memtemp = membuf_create(filesz);
        if( !memtemp ) break;

        recsz = file_read_binary(file, memtemp->buf, memtemp->size);
        if( recsz != (size_t)filesz ) break;

        succeed = true;
    } while(false);

    if( succeed ) membuf_move_from(self, memtemp);

    if( file ) fclose(file);
    membuf_release(memtemp);

    return succeed;
}

bool membuf_save_text(const membuf_t *self, const char *filename)
{
    /**
     * @memberof membuf
     * @brief 將緩衝區內容存入為文字檔案，通常在除錯與人工監視記憶體內容時使用。
     *
     * @param self     Object instance.
     * @param filename Name of file to save information.
     * @return TRUE if succeed; and FALSE if failed.
     */
    FILE *file = NULL;

    if( !self || !filename ) return false;

    file = file_open_writable_text(filename);
    if( !file ) return false;

    file_write_text(file, self->buf, self->size);

    fclose(file);
    return true;
}
