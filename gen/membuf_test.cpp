/*
 * membuf 測試程式
 */
#include <assert.h>
#include <string.h>
#include <stdio.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "minmax.h"
#include "membuf.h"

#ifdef NDEBUG
    #error This test program must work with macro "ASSERT" enabled!
#endif

void test_global_tools(void)
{
    // Data search
    {
        const uint8_t sample[] =
        {
            0x01, 0x05, 0x05, 0x07, 0x02, 0x04, 0x06, 0x08,
            0x01, 0x03, 0x05, 0x07, 0x02, 0x04, 0x06, 0x08,
            0x05, 0x07
        };
        const uint8_t pattern[] = { 0x05, 0x07, 0x02 };

        assert(    2 == (uint8_t*) memfind (sample, sizeof(sample), pattern, sizeof(pattern)) - sample );
        assert(   10 == (uint8_t*) memrfind(sample, sizeof(sample), pattern, sizeof(pattern)) - sample );
        assert( NULL == (uint8_t*) memfind (sample, sizeof(sample), "\x03\x08\x09\x02", 4) );
        assert( NULL == (uint8_t*) memrfind(sample, sizeof(sample), "\x03\x08\x09\x02", 4) );
    }

    // Data search count and replace;
    {
        const uint8_t src[] =
        {
            0x0D, 0x0A,
            's', 't', 'r', 'i', 'n', 'g',
            0x0D, 0x0A,
            'f', 'i', 'l', 'l', 'e', 'd',
            0x0D, 0x0A,
            0x0D, 0x0A,
            'b', 'e', 't', 'w', 'e', 'e', 'n',
        };

        const uint8_t pattern[] = { 0x0D, 0x0A };
        const uint8_t target [] = { 0x1C, 0x1D, 0x1E };

        const uint8_t finaldata[] =
        {
            0x1C, 0x1D, 0x1E,
            's', 't', 'r', 'i', 'n', 'g',
            0x1C, 0x1D, 0x1E,
            'f', 'i', 'l', 'l', 'e', 'd',
            0x1C, 0x1D, 0x1E,
            0x1C, 0x1D, 0x1E,
            'b', 'e', 't', 'w', 'e', 'e', 'n',
        };

        uint8_t buf[64] = {0};

        assert( 4 == memfindcount(src, sizeof(src), pattern, sizeof(pattern)) );

        assert( sizeof(finaldata) == memfindreplace(NULL,
                                                    0,
                                                    src,
                                                    sizeof(src),
                                                    pattern,
                                                    sizeof(pattern),
                                                    target,
                                                    sizeof(target)) );
        assert( sizeof(finaldata) == memfindreplace(buf,
                                                    sizeof(buf),
                                                    src,
                                                    sizeof(src),
                                                    pattern,
                                                    sizeof(pattern),
                                                    target,
                                                    sizeof(target)) );
        assert( 0 == memcmp(buf, finaldata, sizeof(finaldata)) );
    }

    // membuf_calc_recommended_size
    {
        size_t size1 = -1;
        size_t size2 = membuf_calc_recommended_size(size1);
        assert( size2 >= size1 );
        for(size1=0; size1<0xFFFFFF; ++size1)
        {
            size2 = membuf_calc_recommended_size(size1);
            assert( size2 >= size1 );
        }
    }
}

void test_memory_object(void)
{
    // Object create and release
    {
        static const size_t size = 0;
        membuf_t *mem = NULL;

        assert( mem = membuf_create(size) );
        assert( mem->size == size );
        assert( mem->size_total >= size );
        membuf_release_s(NULL);
        membuf_release_s(&mem);
        membuf_release_s(&mem);
        assert( !mem );
    }

    // Object create and import and move
    {
        static const uint8_t testdata[] = {1,3,5,7,9};
        membuf_t *memsrc  = NULL;
        membuf_t *memdest = NULL;

        // Create and import
        assert( memsrc = membuf_create_import(testdata, sizeof(testdata)) );
        assert( memsrc->buf );
        assert( memsrc->size == sizeof(testdata) );
        assert( 0 == memcmp(memsrc->buf, testdata, sizeof(testdata)) );
        // Create and move
        assert( memdest = membuf_create_move(memsrc) );
        assert( memdest->buf );
        assert( memdest->size == sizeof(testdata) );
        assert( 0 == memcmp(memdest->buf, testdata, sizeof(testdata)) );
        assert( memsrc );
        assert( memsrc->buf );
        assert( memsrc->size == 0 );

        membuf_release(memsrc);
        membuf_release(memdest);
    }

    // Object create and clone
    {
        static const uint8_t testdata[] = {1,3,5,7,9};
        membuf_t *memsrc  = NULL;
        membuf_t *memdest = NULL;

        assert( memsrc = membuf_create(sizeof(testdata)) );
        memcpy(memsrc->buf, testdata, sizeof(testdata));

        assert( memdest = membuf_create_clone(memsrc) );
        assert( memdest      != memsrc );
        assert( memdest->buf != memsrc->buf );
        assert( 0 == memcmp(memdest->buf, memsrc->buf, memsrc->size) );

        membuf_release(memsrc);
        membuf_release(memdest);
    }

    // Set zeros
    {
        static const uint8_t zeros[] = "\x0\x0\x0\x0\x0\x0\x0\x0";
        static const size_t size    = sizeof(zeros);
        membuf_t *mem = NULL;

        assert( mem = membuf_create(size) );

        memset(mem->buf, -1, size);
        membuf_set_zeros(mem);
        assert( 0 == memcmp(mem->buf, zeros, size) );

        membuf_release(mem);
    }

    // Resize
    {
        static const uint8_t testdata[] = "\x01\x02\x03\x04\x05\x06\x07\x08";
        static const size_t testsize   = sizeof(testdata);
        membuf_t *mem = NULL;
        size_t   newsize;

        assert( mem = membuf_create(testsize) );
        memcpy(mem->buf, testdata, testsize);

        newsize = testsize + testsize/2;
        assert( membuf_resize(mem, newsize) );
        assert( mem->size == newsize );
        assert( 0 == memcmp(mem->buf, testdata, MIN(newsize,testsize)) );

        newsize = testsize/2;
        assert( membuf_resize(mem, newsize) );
        assert( mem->size == newsize );
        assert( 0 == memcmp(mem->buf, testdata, MIN(newsize,testsize)) );

        membuf_release(mem);
    }

    // Import
    {
        static const uint8_t testdata[] = "\x01\x02\x03\x04\x05\x06\x07\x08";
        membuf_t *mem = NULL;

        assert( mem = membuf_create(128) );

        assert( membuf_import(mem, testdata, sizeof(testdata)) );
        assert( mem->size == sizeof(testdata) );
        assert( 0 == memcmp(mem->buf, testdata, mem->size) );

        membuf_release(mem);
    }

    // Append
    {
        static const uint8_t data_1  [] = "\x01\x02\x03\x04";
        static const uint8_t data_2  [] = "\x05\x06\x07\x08";
        static const uint8_t data_all[] = "\x01\x02\x03\x04\x00\x05\x06\x07\x08";
        membuf_t *mem = NULL;

        assert( mem = membuf_create(0) );

        assert( membuf_import(mem, data_1, sizeof(data_1)) );
        assert( membuf_append(mem, data_2, sizeof(data_2)) );
        assert( mem->size == sizeof(data_all) );
        assert( 0 == memcmp(mem->buf, data_all, mem->size) );

        membuf_release(mem);
    }

    // Move
    {
        static const uint8_t testdata[] = "\x01\x02\x03\x04\x05\x06\x07\x08";
        membuf_t *memsrc  = NULL;
        membuf_t *memdest = NULL;

        assert( memsrc  = membuf_create_import(testdata, sizeof(testdata)) );
        assert( memdest = membuf_create(0)                                 );

        membuf_move_from(memdest, memsrc);
        assert( memdest->buf );
        assert( memdest->size == sizeof(testdata) );
        assert( 0 == memcmp(memdest->buf, testdata, sizeof(testdata)) );
        assert( memsrc->buf );
        assert( memsrc->size == 0 );

        membuf_release(memsrc);
        membuf_release(memdest);
    }

    // Pop front
    {
        static const uint8_t testdata[] = "\x01\x02\x03\x04\x05\x06\x07\x08";
        static const size_t  popsz      = 4;
        membuf_t *mem = NULL;

        assert( mem = membuf_create(0) );

        assert( membuf_import(mem, testdata, sizeof(testdata)) );
        membuf_pop_front(mem, popsz);
        assert( mem->size = sizeof(testdata) - popsz );
        assert( 0 == memcmp(mem->buf, testdata+popsz, sizeof(testdata)-popsz) );

        membuf_pop_front(mem, sizeof(testdata));
        assert( mem->size == 0 );

        membuf_release_s(&mem);
    }

    // Binary file read and write
    {
        static const char    filename[] = "membuf-binary-temp-file";
        static const uint8_t testdata[] = "membuf_t load save test string.";
        membuf_t *mem = NULL;

        // Save file
        assert( mem = membuf_create(0) );
        assert( membuf_import(mem, testdata, sizeof(testdata)) );
        assert( membuf_save_file(mem, filename) );
        memset(mem->buf, 0, mem->size_total);
        membuf_release_s(&mem);

        // Load file
        assert( mem = membuf_create(0) );
        assert( membuf_load_file(mem, filename) );
        assert( mem->size == sizeof(testdata) );
        assert( 0 == memcmp(mem->buf, testdata, mem->size) );
        membuf_release_s(&mem);

        // Create object and load file
        assert( mem = membuf_create_load_file(filename) );
        assert( mem->size == sizeof(testdata) );
        assert( 0 == memcmp(mem->buf, testdata, mem->size) );
        membuf_release_s(&mem);

        // Remove the temporary file
        remove(filename);
    }

    // Text format file output
    {
        static const char    filename[] = "membuf-text-temp-file";
        static const uint8_t testdata[] =
        {
            0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27,   0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F,
            0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47,   0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F,
            0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67,   0x68, 0x69, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F,
        };

        membuf_t *mem = NULL;

        assert( mem = membuf_create_import(testdata, sizeof(testdata)) );
        assert( membuf_save_text(mem, filename) );
        membuf_release_s(&mem);

        printf("Test of the text information output : \n\n");
        assert( mem = membuf_create_load_file(filename) );
        assert( membuf_append(mem, "\x00", 1) );  // Add null terminator
        printf("%s\n", (char*)mem->buf);
        membuf_release_s(&mem);

        remove(filename);
    }
}

int main(void)
{
    test_global_tools();
    test_memory_object();

    return 0;
}
