/**
 * @file
 * @brief     Memory buffer
 * @details   To support simply memory buffer objects to handle the details of buffer operation.
 * @author    王文佑
 * @date      2014.01.20
 * @copyright ZLib Licence
 * @see       http://www.openfoundry.org/of/projects/2419
 */
#ifndef _GEN_MEMBUF_H_
#define _GEN_MEMBUF_H_

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#if defined(__cplusplus) && !defined(GENUTIL_NO_STDCPP)
#include <new>
#include <string>
#endif

#include "static_assert.h"

//----------------------------------------
//---- Global Setting and Tools ----------
//----------------------------------------

#ifdef __cplusplus
extern "C"{
#endif

#ifdef __cplusplus
    #define MEMBUF_ARG_DEFAULT(value) =value
#else
    #define MEMBUF_ARG_DEFAULT(value)
#endif

#define MEMBUF_BLOCKSZ_SMALL sizeof(int)  // 緩衝區尺寸將為此數的倍數
#define MEMBUF_BLOCKSZ_LARGE 512          // 緩衝區塊配置的大單位尺寸
STATIC_ASSERT( MEMBUF_BLOCKSZ_LARGE % MEMBUF_BLOCKSZ_SMALL == 0 );

void* memfind(const void *src, size_t srcsz, const void *pattern, size_t patsz);
void* memrfind(const void *src, size_t srcsz, const void *pattern, size_t patsz);

unsigned memfindcount(const void *src, size_t srcsz, const void *pattern, size_t patsz);
size_t memfindreplace(
    void        *dest,
    size_t      destsz,
    const void  *src,
    size_t      srcsz,
    const void  *pattern,
    size_t      patsz,
    const void  *target,
    size_t      tarsz);

size_t membuf_calc_recommended_size(size_t size);

#ifdef __cplusplus
}  // extern "C"
#endif

//----------------------------------------
//---- Memory Buffer ---------------------
//----------------------------------------

#ifdef __cplusplus
extern "C"{
#endif

/**
 * @class membuf
 * @brief Memory buffer
 *
 * @attention
 * Some rules need to be followed:
 *     @li  Functions with prefix "membuf_init" are using to initialise
 *          an object of membuf_t which is already allocated, and
 *          it needs to call "membuf_destroy" to destroy the object.
 *     @li  Functions with prefix "membuf_create" are using to
 *          allocate and initialise an object, and
 *          it needs to call "membuf_release" to release the object.
 */
typedef struct membuf
{
    // Private

    size_t  size_total; // 緩衝區的實際大小。
                        // 這個數值由本類別所私有使用，一般使用者請勿使用及變更此變數。
    uint8_t *buf_total; // 緩衝區的實際位置。

    // Public

    size_t  size;       ///< @brief 緩衝區被使用的大小(Read Only)。
                        ///< @warning 因為某些緣故，我們提供使用者直接存取這個變數的方式，而不是以函式為之。
                        ///<          但這個變數為唯讀變數，請勿直接更改其值；
                        ///<          欲變動緩衝區大小，請使用 membuf_resize 函式。

    uint8_t *buf;       ///< 被使用的資料緩衝區。

} membuf_t;

void membuf_init(membuf_t *self, size_t size MEMBUF_ARG_DEFAULT(0));
void membuf_init_import(membuf_t *self, const void *buffer, size_t size);
void membuf_init_clone(membuf_t *self, const membuf_t *src);
void membuf_init_move(membuf_t *self, membuf_t *src);
void membuf_destroy(membuf_t *self);

membuf_t*   membuf_create(size_t size MEMBUF_ARG_DEFAULT(0));
membuf_t*   membuf_create_import(const void *buffer, size_t size);
membuf_t*   membuf_create_clone(const membuf_t *src);
membuf_t*   membuf_create_move(membuf_t *src);
membuf_t*   membuf_create_load_file(const char *filename);
void        membuf_release(membuf_t *self);
void        membuf_release_s(membuf_t **self);

void membuf_set_zeros(membuf_t  *self);
bool membuf_resize(membuf_t *self, size_t size);
void membuf_clear(membuf_t *self);
bool membuf_import(membuf_t *self, const void *buffer, size_t size);
bool membuf_append(membuf_t *self, const void *buffer, size_t size);
void membuf_move_from(membuf_t *self, membuf_t *src);
void membuf_pop_front(membuf_t *self, size_t popsz);

int         membuf_compare(const membuf_t *self, const membuf_t *tar);
const void* membuf_find(const membuf_t *self, const membuf_t *pattern);
const void* membuf_rfind(const membuf_t *self, const membuf_t *pattern);
unsigned    membuf_find_count(const membuf_t *self, const membuf_t *pattern);
bool        membuf_find_replace(membuf_t *self, const membuf_t *pattern, const membuf_t *target);

bool membuf_save_file(const membuf_t *self, const char *filename);
bool membuf_load_file(membuf_t *self, const char *filename);
bool membuf_save_text(const membuf_t *self, const char *filename);

#ifdef __cplusplus
}  // extern "C"
#endif

#if defined(__cplusplus) && !defined(GENUTIL_NO_STDCPP)

/**
 * @brief C++ wrapper of @ref membuf
 */
class MemoryBuffer : protected membuf_t
{
public:
    /// @see membuf::membuf_init
    MemoryBuffer(size_t size = 0)
    { membuf_init(this, size); }

    /// @see membuf::membuf_init_import
    MemoryBuffer(const void *buffer, size_t size)
    { membuf_init_import(this, buffer, size); }

    /// @see membuf::membuf_init_clone
    MemoryBuffer(const MemoryBuffer &src)
    { membuf_init_clone(this, &src); }

#if __cplusplus >= 201103L
    /// @see membuf::membuf_init_move
    MemoryBuffer(MemoryBuffer &&src)
    { membuf_init_move(this, &src); }
#endif

    /// @see membuf::membuf_destroy
    ~MemoryBuffer()
    { membuf_destroy(this); }

    MemoryBuffer& operator=(const MemoryBuffer &src)
    { if( !membuf_import(this, src.buf, src.size) ) throw std::bad_alloc(); return *this; }

#if __cplusplus >= 201103L
    MemoryBuffer& operator=(MemoryBuffer &&src)
    { membuf_move_from(this, &src); return *this; }
#endif

public:
    size_t          Size() const { return size; }   ///< Get buffer size.
    const uint8_t*  Buf() const { return buf; }     ///< Get data buffer.
    uint8_t*        Buf() { return buf; }           ///< Get data buffer.

    /// @see membuf::membuf_set_zeros
    void SetZeros()
    { membuf_set_zeros(this); }

    /// @see membuf::membuf_resize
    void Resize(size_t size)
    { if( !membuf_resize(this, size) ) throw std::bad_alloc(); }

    /// @see membuf::membuf_clear
    void Clear()
    { membuf_clear(this); }

    /// @see membuf::membuf_import
    void Import(const void *buffer, size_t size)
    { if( !membuf_import(this,buffer, size) ) throw std::bad_alloc(); }

    /// @see membuf::membuf_append
    void Append(const void *buffer, size_t size)
    { if( !membuf_append(this, buffer, size) ) throw std::bad_alloc(); }

    /// @see membuf::membuf_append
    void Append(const MemoryBuffer &src)
    { if( !membuf_append(this, src.buf, src.size) ) throw std::bad_alloc(); }

    /// @see membuf::membuf_move_from
    void MoveFrom(MemoryBuffer &src)
    { membuf_move_from(this, &src); }

    /// @see membuf::membuf_pop_front
    void PopFront(size_t popsize)
    { membuf_pop_front(this, popsize); }

    /// @see membuf::membuf_compare
    int Compare(const MemoryBuffer &target) const
    { return membuf_compare(this, &target); }

    /// @see membuf::membuf_find
    const void* Find(const MemoryBuffer &pattern) const
    { return membuf_find(this, &pattern); }

    /// @see membuf::membuf_rfind
    const void* RFind(const MemoryBuffer &pattern) const
    { return membuf_rfind(this, &pattern); }

    /// @see membuf::membuf_find_count
    unsigned FindCount(const MemoryBuffer &pattern) const
    { return membuf_find_count(this, &pattern); }

    /// @see membuf::membuf_find_replace
    void FindReplace(const MemoryBuffer &pattern, const MemoryBuffer &target)
    { if( !membuf_find_replace(this, &pattern, &target) ) throw std::bad_alloc(); }

    /// @see membuf::membuf_save_file
    bool SaveFile(const std::string &filename) const
    { return membuf_save_file(this, filename.c_str()); }

    /// @see membuf::membuf_load_file
    bool LoadFile(const std::string &filename)
    { return membuf_load_file(this, filename.c_str()); }

    /// @see membuf::membuf_save_text
    bool SaveText(const std::string &filename) const
    { return membuf_save_text(this, filename.c_str()); }

public:
    bool operator==(const MemoryBuffer &tar) { return 0 == Compare(tar); }  ///< Contents comparison encapsulation.
    bool operator!=(const MemoryBuffer &tar) { return 0 != Compare(tar); }  ///< Contents comparison encapsulation.
    bool operator< (const MemoryBuffer &tar) { return 0 >  Compare(tar); }  ///< Contents comparison encapsulation.
    bool operator> (const MemoryBuffer &tar) { return 0 <  Compare(tar); }  ///< Contents comparison encapsulation.

};

#endif  // __cplusplus && !GENUTIL_NO_STDCPP

#endif
