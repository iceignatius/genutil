/**
 * @file
 * @brief     System time.
 * @details   Encapsulation of functions about system time.
 * @author    王文佑
 * @date      2014.03.09
 * @copyright ZLib Licence
 * @see       http://www.openfoundry.org/of/projects/2419
 *
 * @note Unix Timestamp: Seconds passed from 1970/01/01 00:00:00, and can be a negative value.
 */
#ifndef _GEN_SYSTIME_H_
#define _GEN_SYSTIME_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __GNUC__
#   define GEN_SYSTIME_DEPRECATED(comment)  __attribute__((deprecated(comment)))
#else
#   define GEN_SYSTIME_DEPRECATED(comment)
#endif

void systime_sleep(unsigned ms);
void systime_sleep_awhile(void);
unsigned systime_get_clock_count(void) GEN_SYSTIME_DEPRECATED("Use systime_get_mstime instead!");
unsigned long long systime_get_clocktime(void);

static inline
unsigned long long systime_get_ustime(void)
{
    /// 取得系統從開機到呼叫這個函式時所經過的時間(microseconds)。
    return systime_get_clocktime();
}

static inline
unsigned systime_get_mstime(void)
{
    /// 取得系統從開機到呼叫這個函式時所經過的時間(milliseconds)。
    return systime_get_clocktime() / 1000;
}

int       systime_get_timezone(void);
long long systime_get_utc(void);
long long systime_get_local(void);
long long systime_get_build(void);

#ifdef __cplusplus
}  // extern "C"
#endif

#endif
