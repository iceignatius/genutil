#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#ifdef __linux__
#   include <unistd.h>
#endif

#ifdef _WIN32
#   include <windows.h>
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "timeinf.h"
#include "systime.h"

#define UNUSED(x) ((void)(x))

struct month_info
{
    char name[3+1];
    int value;
};

static long long time_shift_utc;
static long long time_shift_local;

void systime_sleep(unsigned ms)
{
    /**
     * 讓呼叫這個函式的執行緒休眠一段時間。
     * @param ms 欲休眠的時間(milliseconds)。
     */
#if   defined(__linux__)
    static const unsigned ms_max = ((unsigned)-1) / 1000;
    usleep( 1000 * (( ms > ms_max )?( ms_max ):( ms )) );
#elif defined(_WIN32)
    Sleep(ms);
#else
    #error No implementation on this platform!
#endif
}

void systime_sleep_awhile(void)
{
    /**
     * 讓呼叫這個函式的執行緒休眠一段很小的時間，主要的目的是讓執行緒放出 CPU 資源，
     * 對於解決 Busy Wait 的需求很有幫助。
     */
#if   defined(__linux__)
    usleep(1);
#elif defined(_WIN32)
    Sleep(1);
#else
    #error No implementation on this platform!
#endif
}

unsigned systime_get_clock_count(void)
{
    /**
     * 取得系統從開機到呼叫這個函式時所經過的時間(milliseconds)。
     * @note Users will need to link with "rt" library for glibc version before 2.17.
     */
#if   defined(__linux__)

#   if defined(CLOCK_MONOTONIC_COARSE)
    static const clockid_t clockid = CLOCK_MONOTONIC_COARSE;
#   elif defined(CLOCK_MONOTONIC)
    static const clockid_t clockid = CLOCK_MONOTONIC;
#   elif defined(CLOCK_MONOTONIC_RAW)
    static const clockid_t clockid = CLOCK_MONOTONIC_RAW;
#   else
    static const clockid_t clockid = CLOCK_REALTIME;
#   endif

    struct timespec tp;
    int gettime_result = clock_gettime(clockid, &tp);
    assert( gettime_result == 0 );
    UNUSED(gettime_result);

    return tp.tv_sec * 1000ULL + tp.tv_nsec / (1000*1000);

#elif defined(_WIN32)

    #if WINVER > 0x0600
    return GetTickCount64();
    #else
    return GetTickCount();
    #endif

#else
    #error No implementation on this platform!
#endif
}

unsigned long long systime_get_clocktime(void)
{
    /**
     * 取得系統從開機到呼叫這個函式時所經過的時間(microseconds)。
     * @note Users will need to link with "rt" library for glibc version before 2.17.
     */
#if defined(__linux__)

#   if defined(CLOCK_MONOTONIC_COARSE)
    static const clockid_t clockid = CLOCK_MONOTONIC_COARSE;
#   elif defined(CLOCK_MONOTONIC)
    static const clockid_t clockid = CLOCK_MONOTONIC;
#   elif defined(CLOCK_MONOTONIC_RAW)
    static const clockid_t clockid = CLOCK_MONOTONIC_RAW;
#   else
    static const clockid_t clockid = CLOCK_REALTIME;
#   endif

    struct timespec tp;
    int gettime_result = clock_gettime(clockid, &tp);
    assert( gettime_result == 0 );
    UNUSED(gettime_result);

    return tp.tv_sec * (1000ULL*1000ULL) + tp.tv_nsec / 1000;

#elif defined(_WIN32)

    static unsigned long long freq = 0;
    if( !freq )
    {
        LARGE_INTEGER value;
        bool mustsucc = QueryPerformanceFrequency(&value);
        assert(mustsucc);
        (void) mustsucc;    // Avoid the unused warning.

        freq = value.QuadPart;
    }

    unsigned long long count;
    {
        LARGE_INTEGER value;
        bool mustsucc = QueryPerformanceCounter(&value);
        assert(mustsucc);
        (void) mustsucc;    // Avoid the unused warning.

        count = value.QuadPart;
    }

    return count * (1000*1000) / freq;

#else
    #error No implementation on this platform!
#endif
}

static
timeinf_t timeinf_from_stdtm(struct tm *stdtm)
{
    timeinf_t timeinf;

    timeinf.year   = stdtm->tm_year + 1900;
    timeinf.month  = stdtm->tm_mon  + 1;
    timeinf.day    = stdtm->tm_mday;
    timeinf.hour   = stdtm->tm_hour;
    timeinf.minute = stdtm->tm_min;
    timeinf.second = stdtm->tm_sec;

    return timeinf;
}

static
void calc_time_shift(void)
{
    time_t    time_curr     = time(NULL);
    timeinf_t timeinf_utc   = timeinf_from_stdtm(gmtime   (&time_curr));
    timeinf_t timeinf_local = timeinf_from_stdtm(localtime(&time_curr));
    long long time_utc      = timeinf_to_uxtime(&timeinf_utc);
    long long time_local    = timeinf_to_uxtime(&timeinf_local);

    time_shift_utc   = time_utc   - time_curr;
    time_shift_local = time_local - time_curr;
}

int systime_get_timezone(void)
{
    /**
     * 取得本地的時區。
     * @return 傳回本地時間與 UTC 時間的差距(Hours)。
     */
    static bool inited = false;
    static int  timezone;

    if( !inited )
    {
        calc_time_shift();
        timezone = (int)( ( time_shift_local - time_shift_utc )/3600 );

        inited = true;
    }

    return timezone;
}

long long systime_get_utc(void)
{
    /**
     * 取得當前的 UTC 時間。
     * @return UNIX time stamp (UTC).
     */
    static bool inited = false;

    if( !inited )
    {
        calc_time_shift();
        inited = true;
    }

    return time(NULL) + time_shift_utc;
}

long long systime_get_local(void)
{
    /**
     * 取得當前的本地時間。
     * @return UNIX time stamp (local).
     */
    static bool inited = false;

    if( !inited )
    {
        calc_time_shift();
        inited = true;
    }

    return time(NULL) + time_shift_local;
}

static
int compare_month_info(const struct month_info *l, const struct month_info *r)
{
    return strcmp(l->name, r->name);
}

static
unsigned get_month_from_str(const char *str)
{
    static struct month_info month_list[] =
    {
        { "Jan", 1 },
        { "Feb", 2 },
        { "Mar", 3 },
        { "Apr", 4 },
        { "May", 5 },
        { "Jun", 6 },
        { "Jul", 7 },
        { "Aug", 8 },
        { "Sep", 9 },
        { "Oct", 10 },
        { "Nov", 11 },
        { "Dec", 12 },
    };

    static bool inited = false;

    if( !inited )
    {
        qsort(
            month_list,
            12,
            sizeof(month_list[0]),
            (int(*)(const void*,const void*)) compare_month_info);
        inited = true;
    }

    struct month_info *entry =
        bsearch(
            str,
            month_list,
            12,
            sizeof(month_list[0]),
            (int(*)(const void*,const void*)) compare_month_info);
    return entry ? entry->value : 1;
}

long long systime_get_build(void)
{
    /**
     * 取得程式建置的本地時間。
     * @return UNIX time stamp (local).
     */
    static long long build_time = 0;
    static bool inited = false;

    if( !inited )
    {
        calc_time_shift();

        char year[]     = { __DATE__[7], __DATE__[8], __DATE__[9], __DATE__[10], 0 };
        char month[]    = { __DATE__[0], __DATE__[1], __DATE__[2], 0 };
        char day[]      = { __DATE__[4], __DATE__[5], 0 };
        char hour[]     = { __TIME__[0], __TIME__[1], 0 };
        char minute[]   = { __TIME__[3], __TIME__[4], 0 };
        char second[]   = { __TIME__[6], __TIME__[7], 0 };

        timeinf_t timeinf =
        {
            .year   = strtoul(year, NULL, 10),
            .month  = get_month_from_str(month),
            .day    = strtoul(day, NULL, 10),
            .hour   = strtoul(hour, NULL, 10),
            .minute = strtoul(minute, NULL, 10),
            .second = strtoul(second, NULL, 10),
        };

        build_time = timeinf_to_uxtime(&timeinf);

        inited = true;
    }

    return build_time;
}
