/**
 * @file
 * @brief     Hash.
 * @details   To support simple hashing functions.
 * @author    王文佑
 * @date      2014.01.20
 * @copyright ZLib Licence
 * @see       http://www.openfoundry.org/of/projects/2419
 */
#ifndef _GEN_HASH_H_
#define _GEN_HASH_H_

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

uint8_t hash_pearson(const void *src, size_t size);
uint16_t hash_crc16_ccitt(const void *src, size_t size);
uint16_t hash_crc16_ibm(const void *src, size_t size);
uint32_t hash_crc32(const void *src, size_t size);
uint32_t hash_jenkins(const void *src, size_t size);
uint32_t hash_fnv32(const void *src, size_t size);
uint32_t hash_murmur3_32(const void *src, size_t size, uint32_t seed);
uint64_t hash_murmur3_64(const void *src, size_t size, uint64_t seed);

#ifdef __cplusplus
}  // extern "C"
#endif

#endif
